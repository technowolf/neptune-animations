# Contributing

When contributing to this repository, please first discuss the change you wish 
to make via issue,email, or any other method with the developers of this repository 
before making a change.
Alternatively you can also contact maintainers of this repo: [Daksh](https://t.me/daksh7011) or [Siddhi Agrawal](https://t.me/echo_siddhi)

>NOTE: It is always recommanded to open issue instead of sending PM to developers.

## Getting Started
  Follow steps below to get started with development or contribution.

  1.  Open Terminal
  2. `mkdir neptune-animations`
  3. `cd neptune-animations`
  4. Initialize a local git repo`git init`
  5. Add remote `git remote add origin https://gitlab.com/daksh7011/neptune-animations.git`
  6. Change local branch to dev `git checkout -b <your_branch>`
  6. Pull repo from master branch`git pull origin master` *
  7. Set your name for commits `git config --global user.name "YourNameGoesHere"`
  8. Now set your email `git config --global user.email you@domainName.extention`

***You can pull from another branch which is ahead in commits from master.**

>Make sure that you have added remote origin by running `git remote -v`

**Thats all!** Now you must have a properly configured an cloned repository.

## Pushing your branch to repo
   Follow steps below to start pushing changes you have made into your new branch
   
   1. In terminal in **neptune-animations** folder , `git push origin <your_branch>`
   2. Enter Your username and password for gitlab
   3. Check for success message in terminal
   4. After successful push, login to your gitlab account in browser.
   5. Open [repository] (https://gitlab.com/daksh7011/neptune-animations)
   6. Create merge request and wait for maintainer to accept it and merge it with master branch.
       
***If there is any push conflict, you can search for it or send PM to maintainers.**

Always feel free to create issues related to this project.
**Good luck**

Last updated on 08/19/2018: 22:00 by [Daksh](https://gitlab.com/daksh7011)