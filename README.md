# Neptune - CSS Hover Animations Library

Neptune Animations is a CSS animation library for web elements.

There are 21 styles currently available to use.

Check out Demo to see all the styles.

## [Demo](https://daksh7011.com/neptune/)

## [Demo](https://daksh7011.com/demo/neptune/)

# [Changelog](CHANGELOG)

# [Contribution guideline for this project](CONTRIBUTING.md)

# License

Copyright 2018 Daksh Desai, daksh7011.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
